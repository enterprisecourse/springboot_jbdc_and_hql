package com.example.demo.repositories.sessionsuser;


public interface MainRepositorySessionsUser<EntityUser, IdUser, EntitySubscription, IdSubscription, Date, Integer, String> {

    String sessionRepositoryCheckSessionUserByIdUser(String idUser);

    void sessionRepositoryAddNoticeEntryUser(String dateEnterStart, Integer time, String idUserSubscription, String idUser, String dateEnterStartLong);

    public int sessionRepositoryGetOverallTimeSubscription(String idUser);

    void sessionRepositoryDeleteTableVisit(String idUser);

    public String repositoryGetTimeStartEntryApplication(String idUser);

    void repositoryUpdateTableVisit(java.lang.String idUser, java.lang.String valueOf, java.util.Date dateEnterFinish, int timefinal);


}
