package com.example.demo.repositories.sessionsuser;

import com.example.demo.entiti.Subscription;
import com.example.demo.entiti.User;
import com.example.demo.entiti.Visit;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;


@Slf4j
@RequiredArgsConstructor
@Repository
public class SessionRepositoryClassJPA implements SessionsRepository {

    private final SessionFactory sessionFactory;

    @Override
    public String sessionRepositoryCheckSessionUserByIdUser(String idUser) {

        List<String> idSessionsUserExit = new ArrayList<>();
        List<String> idSessionsUserStart = new ArrayList<>();

        Long ID = Long.valueOf(idUser);
        List<Subscription> listentry = new ArrayList<>();
        Visit visit;

        Session session = sessionFactory.openSession();

        Optional optional1 = session.createQuery("SELECT v FROM Visit v where v.user.id=:ID", Visit.class)
                .setParameter("ID", ID).list().stream().findFirst();



        String statusCheck = "";

        if (optional1.isPresent()) {

            visit = (Visit) optional1.get();

            String dateExite = visit.getDateExite();
            String dateEntry = visit.getDateEntry();


            if (dateEntry != null && dateExite == null) {
                statusCheck = "Auth-ok/Exit-no";

            }

            if (dateEntry == null && dateExite != null) {
                statusCheck = "Auth-no/Exit-ok";

            }

            if (dateExite == null && dateEntry == null) {
                statusCheck = "Auth-no/Exit-no";

            }


        } else {

            statusCheck = "Auth-no/Exit-no";


        }

        return statusCheck;


    }

    public void sessionRepositoryAddNoticeEntryUser(String dateEnterStart, Integer time, String idUserSubscription, String idUser, String dateEnterStartLong) {



        Long idUs = Long.valueOf(Integer.parseInt(idUser));
        User user;

        Session session = sessionFactory.openSession();
        Optional<User> optional = session.createQuery("SELECT v FROM User v where v.id=:idUs", User.class)
                .setParameter("idUs", idUs).list().stream().findFirst();


        if (optional.isPresent()) {
            user = optional.get();

            Visit visit = new Visit().setDateEntry(dateEnterStart)
                    .setOverallTime(time)
                    .setIdSubscription(Integer.parseInt(idUserSubscription)).setDateEntrylong(dateEnterStartLong);


            visit.setUser(user);
            System.out.println("visit " + visit);
            session.getSession().save(visit);

        }


    }

    public int sessionRepositoryGetOverallTimeSubscription(String idUser) {

        Long ID = Long.valueOf(idUser);

        Session session1 = sessionFactory.openSession();

        Visit visit1 = null;
        int timeStartEntry;


        Optional<Visit> optional1 = session1.createQuery("SELECT v FROM Visit v where v.user.id=:ID", Visit.class)
                .setParameter("ID", ID).list().stream().findFirst();

        if (optional1.isPresent()) {
            visit1 = optional1.get();
            timeStartEntry = visit1.getOverallTime();
        } else {
            timeStartEntry = 0;
        }


        return timeStartEntry;

    }

    @Override
    public void sessionRepositoryDeleteTableVisit(String idUser) {

        Long idUs = Long.parseLong(idUser);// преобразовую в лонг для поиска в БД
        Visit visitOld;

        Session session = sessionFactory.openSession();
        Transaction tran = session.beginTransaction();
        Visit optional = session.createQuery("SELECT v FROM Visit v where v.user.id=:idUs", Visit.class)
                .setParameter("idUs", idUs).uniqueResult();

        //зит чтобы его удалить
        System.out.println("optional "+optional);

        session.remove(optional);
        tran.commit();


    }

    @Override
    public String repositoryGetTimeStartEntryApplication(String idUser) {

        Long ID = Long.valueOf(idUser);

        Session session1 = sessionFactory.openSession();

        Visit visit1 = null;
        String timeStartEntry;


        Optional<Visit> optional1 = session1.createQuery("SELECT v FROM Visit v where v.user.id=:ID", Visit.class)
                .setParameter("ID", ID).list().stream().findFirst();

        if (optional1.isPresent()) {
            visit1 = optional1.get();
            timeStartEntry = visit1.getDateEntrylong();
        } else {
            timeStartEntry = "";
        }


        return timeStartEntry;
    }
    @Override
    public void repositoryUpdateTableVisit(String idUser, String DateExitelong, Date dateExiteFinish, int timefinal) {


        Long idUs = Long.parseLong(idUser);// преобразовую в лонг для поиска в БД

        Session session = sessionFactory.openSession();
        Transaction tran = session.beginTransaction();
        Visit visit = session.createQuery("SELECT v FROM Visit v where v.user.id=:idUs", Visit.class)
                .setParameter("idUs", idUs).uniqueResult();

        sessionRepositoryDeleteTableVisit(idUser);



            String DataExite = String.valueOf(dateExiteFinish);
            String DataExiteLong = DateExitelong;

            visit.setDateExite(DataExite);
            visit.setDateExitelong(DataExiteLong);

            saveVisit(visit);


    }

    private void saveVisit(Visit visit) {


        System.out.println("Visit "+visit);

        Session session = sessionFactory.openSession();

        session.getSession().save(visit);
    }

}



