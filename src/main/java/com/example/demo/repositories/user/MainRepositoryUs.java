package com.example.demo.repositories.user;

import com.example.demo.entiti.User;

import java.util.Date;
import java.util.List;

public interface MainRepositoryUs<EntityUser , IdUser,EntitySubscription,IdSubscription, Date,Integer,String> {

    public void RepositoryRegistrationUserByUser(EntityUser user); // регистрация пользователя в таблице users

    public String RepositoryFindUserIdByUserName(EntityUser username); // найти Id пользователя в таблице users по имени

    public List<EntityUser> RepositoryFindUserByUserName(EntityUser entityUser); // найти пользователя в таблице users по имени

    public int RepositoryGetOverallTime(String idUser); // получаем полное время клиента из таблицы

    public String RepositoryCheckSessionUserByIdUser(String userID);

    void RepositoryDeleteTableByUserId(String idUser);

    String RepositoryGetDataStartEntry(String idUser);


}
