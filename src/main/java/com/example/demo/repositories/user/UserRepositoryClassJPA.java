package com.example.demo.repositories.user;
import com.example.demo.entiti.User;
import com.example.demo.entiti.Visit;
import lombok.RequiredArgsConstructor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.*;


@RequiredArgsConstructor
@Repository
public class UserRepositoryClassJPA implements UserRepository {

    private final SessionFactory sessionFactory;


    @Override
    public void RepositoryRegistrationUserByUser(User user) {
        Session session = sessionFactory.openSession();
        session.getSession().save(user);
    }

    @Override
    public String RepositoryFindUserIdByUserName(User user) {

        List<String> list = new ArrayList<>();

        String login = user.getLogin();
        List<User> listusers = new ArrayList<>();

        Session session = sessionFactory.openSession();
        Optional<User> optional = session.createQuery("SELECT v FROM User v where v.login=:login", User.class)
                .setParameter("login", login).list().stream().findFirst();
        User user1 = optional.get();

        list.add(String.valueOf(user1.getId()));

        if (list.size() == 0) {
            return "0";
        } else {
            return list.get(0);
        }

    }

    @Override
    public List<User> RepositoryFindUserByUserName(User user) {

        String login = user.getLogin();
        User user1;

        List<User> listusers = new ArrayList<>();

        Session session = sessionFactory.openSession();
        Optional<User> optional = session.createQuery("SELECT v FROM User v where v.login=:login", User.class)
                .setParameter("login", login).list().stream().findFirst();

        if(optional.isPresent()){
            user1 = optional.get();
            listusers.add(user1);
        }



        return listusers;
    }

    @Override
    public int RepositoryGetOverallTime(String idUser) {


        int idUs = Integer.parseInt(idUser);

        Session session = sessionFactory.openSession();

        Optional<Visit> optional = session.createQuery("SELECT Visit FROM User u where u.login=:idUs", Visit.class)
                .setParameter("idUs", idUs).list().stream().findFirst();

        Visit visit = optional.get();
        System.out.println("visit.getOverallTime(); " + visit.getOverallTime());

        return 0;

    }

    public String RepositoryCheckSessionUserByIdUser(String userID) {
        List<String> listentry = new ArrayList<>();
        List<String> listexite = new ArrayList<>();

        Session session = sessionFactory.openSession();
        int idUs = Integer.parseInt(userID);

        Optional<Visit> optional = session.createQuery("SELECT Visit FROM User u where u.login=:idUs", Visit.class)
                .setParameter("idUs", idUs).list().stream().findFirst();

        System.out.println("optional " + optional);

        return "";

    }

    @Override
    public void RepositoryDeleteTableByUserId(String idUser) {

        Session session = sessionFactory.openSession();
        Session session1 = sessionFactory.openSession();

        int idUs = Integer.parseInt(idUser);
        Optional<Visit> visit = session.createQuery("SELECT Visit FROM User u where u.id=:idUs", Visit.class)
                .setParameter("idUs", idUs).list().stream().findFirst();

        Visit visit1 = visit.get();
        Long iDVisit = visit1.getId();

        session1.createQuery("delete from Visit where id=:iDVisit")
                .setParameter("iDVisit", iDVisit);

    }

    @Override
    public String RepositoryGetDataStartEntry(String idUser) {
        List<String> listTime = new ArrayList<>();

        Session session1 = sessionFactory.openSession();

        int idUs = Integer.parseInt(idUser);
        Optional<Visit> visit = session1.createQuery("SELECT Visit FROM User u where u.login=:idUs", Visit.class)
                .setParameter("idUs", idUs).list().stream().findFirst();

        Visit visit1 = visit.get();
        String dataStart = visit1.getDateEntry();

        listTime.add(dataStart);

        return listTime.get(0);
    }



}
