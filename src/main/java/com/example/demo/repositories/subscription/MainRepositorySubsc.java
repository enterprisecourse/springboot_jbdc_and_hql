package com.example.demo.repositories.subscription;

import java.util.Date;
import java.util.List;

public interface MainRepositorySubsc<EntityUser , IdUser,EntitySubscription,IdSubscription, Date,Integer,String> {

    public void repositoryAddSubscription(EntitySubscription entitySubscription, String idSubscription); // добавить абонемент в таблицу subscription

    public String repositoryFindSubscriptionIdByUserId(String IdUser); // получить iD абонемента по имени пользователя

    public List<EntitySubscription> repositoryFindSubscriptionUserId(String userID); // найти абонемент по IDUser

    public void repositoryAddSubscriptionAndUser(EntitySubscription entitySubscription, java.lang.String IDUser); // добавить абонемент и пользователя в таблицу usersSubscription

}
