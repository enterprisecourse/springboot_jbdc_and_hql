package com.example.demo.repositories.subscription;

import com.example.demo.entiti.Subscription;
import com.example.demo.entiti.User;
import com.example.demo.entiti.UsersSubscription;
import com.example.demo.entiti.Visit;
import lombok.RequiredArgsConstructor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Repository
public class SubscriptionRepositoryClassJPA implements SubscriptionRepository {

    private final SessionFactory sessionFactory;


    @Override
    public List<Subscription> repositoryFindSubscriptionUserId(String userID) {

        Long ID = Long.valueOf(userID);
        List<Subscription> listentry;
        User user;

        Session session = sessionFactory.openSession();

        List<Subscription> optional1 = session.createQuery("SELECT v FROM Subscription v where v.user.id=:ID", Subscription.class)
                .setParameter("ID", ID).list().stream().collect(Collectors.toList());

        listentry = optional1;

        System.out.println("listentry " + listentry);


        return listentry;

    }

    @Override
    public void repositoryAddSubscription(Subscription subscription, String IdUser) {

        Session session1 = sessionFactory.openSession();
        Long idUs = Long.valueOf(IdUser);
        Subscription subscription1 = subscription;


        Optional<User> optional = session1.createQuery("SELECT v FROM User v where v.id=:idUs", User.class)
                .setParameter("idUs", idUs).list().stream().findFirst();


        User user = optional.get();
        subscription1.setUser(user);

        saveSubs(subscription1);


    }


    private void saveSubs(Subscription subscription) {


        Session session = sessionFactory.openSession();

        session.getSession().save(subscription);
    }


    private void SubscriptionAndUser(UsersSubscription usersSubscription) {


        Session session = sessionFactory.openSession();

        session.getSession().save(usersSubscription);
    }

    @Override
    public String repositoryFindSubscriptionIdByUserId(String IdUser) {

        Long ID = Long.valueOf(IdUser);
        List<Subscription> listentry;
        User user;
        Subscription subscription;

        Session session = sessionFactory.openSession();

        Optional optional1 = session.createQuery("SELECT v FROM Subscription v where v.user.id=:ID", Subscription.class)
                .setParameter("ID", ID).list().stream().findFirst();

        if (optional1.isPresent()) {

            subscription = (Subscription) optional1.get();
            return String.valueOf(subscription.getId());

        }


        return "";

    }

    @Override
    public void repositoryAddSubscriptionAndUser(Subscription subscription, String IDUser) {

        Session session = sessionFactory.openSession();
        Long idUs = Long.valueOf(IDUser);


        Optional<User> optinal = session.createQuery("SELECT v FROM User v where v.id=:idUs", User.class)
                .setParameter("idUs", idUs).list().stream().findFirst();


        User us = optinal.get();

        UsersSubscription usersSubscription = new UsersSubscription();
        usersSubscription.setUser(us);
        usersSubscription.setState("active");
        usersSubscription.setSubscription(subscription);
        usersSubscription.setPurchasedDate(subscription.getDatePurchase());

        System.out.println("UsersSubscription "+usersSubscription);


        SubscriptionAndUser(usersSubscription);



    }


}



