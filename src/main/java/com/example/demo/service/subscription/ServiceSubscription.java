package com.example.demo.service.subscription;
import com.example.demo.entiti.Subscription;
import com.example.demo.repositories.subscription.SubscriptionRepositoryClass;
import com.example.demo.repositories.subscription.SubscriptionRepositoryClassJPA;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;


@Service
public class ServiceSubscription implements MainServiceSubscription {

    @Autowired
    SubscriptionRepositoryClass subscriptionRepositoryClass;
    @Autowired
    SubscriptionRepositoryClassJPA repositoryClassJPA;

    @Override
    public List<Subscription> ServiceSubscrFindSubscriptionBySubscriptionId(String IdSubscription) {
        return null;
    }

    @Override
    public List<Subscription> ServiceSubscrFindSubscriptionUserId(String userID) {
        return repositoryClassJPA.repositoryFindSubscriptionUserId(userID);
    }

    @Override
    public void ServiceSubscrAddSubscriptionAndUser(Subscription subscription, String IDUser) {
        repositoryClassJPA.repositoryAddSubscriptionAndUser(subscription,IDUser);
    }

    @Override
    public String ServiceFindSubscriptionIdByUserId(String IdUser) {
        return repositoryClassJPA.repositoryFindSubscriptionIdByUserId(IdUser);
    }

    @Override
    public void ServiceSubscrAddSubscription(Subscription subscription, String idUser) {
        repositoryClassJPA.repositoryAddSubscription(subscription,idUser);
    }




}


