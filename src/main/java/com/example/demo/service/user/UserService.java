package com.example.demo.service.user;

import com.example.demo.entiti.User;
import com.example.demo.repositories.sessionsuser.SessionRepositoryClass;
import com.example.demo.repositories.user.UserRepositoryClass;
import com.example.demo.repositories.user.UserRepositoryClassJPA;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class UserService implements MainServiceUser {

    @Autowired
    UserRepositoryClass repositoryUser;

    @Autowired
    UserRepositoryClassJPA userRepositoryClassJPA;

    @Autowired
    SessionRepositoryClass sessionRepositoryClass;


    @Override
    public void ServiceUserRegistrationUserByIdLogin(User user) {
        userRepositoryClassJPA.RepositoryRegistrationUserByUser(user);
    }

    @Override
    public boolean ServiceUserFindUserByUserName(User user) throws Exception {


        boolean status = false;
        List<User> visitList = userRepositoryClassJPA.RepositoryFindUserByUserName(user);

        if (visitList.size() > 0) {
            status=true;
        }

        if (visitList.size() == 0) {
            status=false;
        }

        return status;
    }

    @Override
    public String ServiceUserFindUserIdByUserName(User user) throws Exception {

        String UserID = (String) userRepositoryClassJPA.RepositoryFindUserIdByUserName(user);
        if(UserID.equals("0")){
            throw new Exception("Пользователь с таким логином Не в базе");
        }

        return (String) userRepositoryClassJPA.RepositoryFindUserIdByUserName(user);

    }

    @Override
    public String ServiceCheckSessionUserByIdUser(String userID) {

        return userRepositoryClassJPA.RepositoryCheckSessionUserByIdUser(userID);
    }

    @Override
    public int ServiceGetOverallTimeUser(String idUser) {
        return userRepositoryClassJPA.RepositoryGetOverallTime(idUser);
    }

    @Override
    public String ServiceGetDataStartEntry(String idUser) {
        return userRepositoryClassJPA.RepositoryGetDataStartEntry(idUser);
    }






}
