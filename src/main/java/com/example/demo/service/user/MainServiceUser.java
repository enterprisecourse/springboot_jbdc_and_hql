package com.example.demo.service.user;

import com.example.demo.entiti.User;

import java.util.Date;

public interface MainServiceUser {



    public void ServiceUserRegistrationUserByIdLogin(User user); // регистрация пользователя в таблице users

    public boolean ServiceUserFindUserByUserName(User user) throws Exception; // найти пользователя в таблице users по имени

    public String ServiceUserFindUserIdByUserName(User user) throws Exception; // найти Id пользователя в таблице users по имени


    String ServiceCheckSessionUserByIdUser(String userID);

    int ServiceGetOverallTimeUser(String idUser);

    String ServiceGetDataStartEntry(String idUser);


}
