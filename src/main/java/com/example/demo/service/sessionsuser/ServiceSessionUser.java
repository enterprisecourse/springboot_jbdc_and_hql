package com.example.demo.service.sessionsuser;

import com.example.demo.repositories.sessionsuser.SessionRepositoryClass;
import com.example.demo.repositories.sessionsuser.SessionRepositoryClassJPA;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class ServiceSessionUser implements MainServiceSessionUser {

    @Autowired
    SessionRepositoryClass sessionRepositoryClass;

    @Autowired
    SessionRepositoryClassJPA repositoryClassJPA;


    @Override
    public String ServiceSessionCheckSessionUserByIdUser(String userID) {
        return repositoryClassJPA.sessionRepositoryCheckSessionUserByIdUser(userID);
    }

    @Override
    public void ServiceSessionAddNoticeEntryUser(String dateEnterStart, Integer time, String idUserSubscription, String idUser, String valueOf) {

        repositoryClassJPA.sessionRepositoryAddNoticeEntryUser(dateEnterStart,time,idUserSubscription,idUser,valueOf);
    }

    @Override
    public int ServiceSessionGetOverallTimeUser(String idUser) {
        return repositoryClassJPA.sessionRepositoryGetOverallTimeSubscription(idUser);


    }

    @Override
    public void ServiceSessionDeleteTable(String idUser) {

        repositoryClassJPA.sessionRepositoryDeleteTableVisit(idUser);

    }

    @Override
    public String ServiceSessionGetDataStartEntry(String idUser) {

        return repositoryClassJPA.repositoryGetTimeStartEntryApplication(idUser);


    }









    @Override
    public boolean ServiceSessionCheckTime(int overallTime, double minutes) {

        if (minutes > overallTime) {
            return false;
        }

        return true;
    }

    @Override
    public void ServiceSessionUpdateTable(String idUser, String dateEnterFinishLong, Date dateEnterFinish, int timefinal) {

        repositoryClassJPA.repositoryUpdateTableVisit(idUser, dateEnterFinishLong, dateEnterFinish,timefinal);


    }

}
