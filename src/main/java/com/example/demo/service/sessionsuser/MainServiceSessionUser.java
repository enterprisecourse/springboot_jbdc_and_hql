package com.example.demo.service.sessionsuser;

import java.util.Date;

public interface MainServiceSessionUser {


    String ServiceSessionCheckSessionUserByIdUser(String userID);

    void ServiceSessionAddNoticeEntryUser(String dateEnterStart, Integer time, String idUserSubscription, String idUser, String valueOf);

    public int ServiceSessionGetOverallTimeUser(String idUser);

    void ServiceSessionDeleteTable(String idUser);

    String ServiceSessionGetDataStartEntry(String idUser);

    public boolean ServiceSessionCheckTime(int overallTime, double minutes);

    void ServiceSessionUpdateTable(String idUser, String valueOf, Date dateEnterFinish, int timefinal);


}
