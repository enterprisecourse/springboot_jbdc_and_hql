package com.example.demo.entiti;


import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;

@Accessors(chain = true)
@Data
@Entity
@Table(name = "userssubscription")
public class UsersSubscription {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_userssubscription")
    private Long id;

    @Column(name = "userssubscription_purchase_date")
    String purchasedDate;
    @Column(name = "userssubscription_state")
    String state;


    @OneToOne()
    @JoinColumn(name = "id_users")
    private User user;

    @OneToOne()
    @JoinColumn(name = "id_subscription")
    private Subscription subscription;


}
