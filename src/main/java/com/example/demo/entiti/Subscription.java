package com.example.demo.entiti;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;


@Data
@Accessors(chain = true)
@Entity
@Table(name = "subscription")
public class Subscription {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_subscription")
    private Long id;
    @Column(name = "subscription_name")
    private String names;
    @Column(name = "subscription_time")
    private Integer time;
    @Column(name = "subscription_price")
    private String price;
    @Column(name = "subscription_datepurchase")
    private String DatePurchase;

    @OneToOne()
    @JoinColumn(name = "id_users")
    private User user;



}
