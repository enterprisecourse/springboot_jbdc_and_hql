package com.example.demo.entiti;


import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;


@Accessors(chain = true)
@Data
@Entity
@Table(name = "visit")
public class Visit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_visit")
    private Long id;
    @Column(name = "date_entry")
    private String dateEntry;
    @Column(name = "date_entrylong")
    private String dateEntrylong;
    @Column(name = "date_exite")
    private String dateExite;

    @Column(name = "date_exitelong")
    private String dateExitelong;

    @Column(name = "overall_time")
    private int overallTime;

    @Column(name = "id_subscription")
    private int idSubscription;

    @OneToOne()
    @JoinColumn(name = "id_users")
    private User user;


}
