package com.example.demo.entiti;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.Properties;

@Accessors(chain = true)
@Data
@Entity
@Table(name = "users")
public class User {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_users")
    private Long id;
    @Column(name = "user_name")
    private String login;
    @Column(name = "user_password")
    private String password;
    @Column(name = "user_entrydata")
    private String entryData;


}
