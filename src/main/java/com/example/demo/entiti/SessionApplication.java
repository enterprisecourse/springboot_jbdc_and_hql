package com.example.demo.entiti;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
@Data
public class SessionApplication {


    public Date getTime(){
        return new Date();
    }


}
